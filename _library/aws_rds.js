/**
 * AWS RDS Module.
 *
 * Consists of useful functions and methods to
 * work with AWS RDS via AWS SDK.
 */

/**
 * NPM Modules.
 *
 * Documentation:
 * @module aws_requests  - ./aws_requests.js, custom module with request
 *                         methods to AWS Services.
 * @module user_requests - ./user_requests.js, custom module with methods
 *                         which that use User input.
 */
const aws_requests = require('./aws_requests');
const user_requests = require('./user_requests');

/**
 * Exports Module Functions.
 */
module.exports = {

    /**
     * Get List of DB Instances.
     *
     * @return {Promise<DBInstanceMessage>}
     *      The list of db instances names.
     */
    list_db_instances: () => aws_requests.rds.get_db_instances().then(data =>
            data.map(key => key.DBInstanceIdentifier))
    ,

    /**
     * Create Database Instance.
     * Asks to input a Name for the New DB Instance
     * and, makes a request to AWS RDS to create it.
     *
     * @param {any} params
     *      The parameters for createDBInstance() AWS SDK Request.
     *
     * @return {Promise<Neptune.DBInstance>}
     *      The data of a new DB Instance.
     */
    create_db_instance: function (params={}) {

        params = {
            ..._render_create_db_params(),
            ...params
        };

        return new Promise(resolve => resolve(this.list_db_instances()))
            .then(listOfDbInstances => user_requests.input_new_key(
                'Please, input a DBInstanceIdentifier (Name) for the NEW DB Instance: ',
                listOfDbInstances
                )
                .then(newDbInstanceName => {

                    params.DBInstanceIdentifier = newDbInstanceName;

                    console.log(`Setting a Master User Password for a NEW DB Instance - ${params.DBInstanceIdentifier}`);
                })
            )
            .then(() => user_requests.input_passwd().then(masterUserPasswd => params.MasterUserPassword = masterUserPasswd))
            .then(() => {

                console.log({'Create DB Instance Request Params': params});

                return user_requests.confirm_solution(`Create a NEW DB Instance - ${params.DBInstanceIdentifier}`)
                .then(response => {

                    if (response === false)
                        throw Error('The solution was NOT confirmed. The process was stopped.');

                });
            })
            .then(() => aws_requests.rds.create_db_instance(params).then(data => {

                console.log('[!] A NEW Database is asynchronously creating in AWS RDS!');

                return data.DBInstance;
            }));
    },

    /**
     * Create Read Replica Instance.
     * Waits for availability of source db instance,
     * before creating a read replica db instance.
     * After creating, it waits for availability of
     * read replica db instance.
     *
     * @param {Object} sourceDbInstance
     *      The data of the source DB Instance.
     * @param {any} params
     *      The parameters for createDBInstanceReadReplica() AWS SDK Request.
     *      Required parameter: DBInstanceIdentifier.
     *
     * @return {Promise<DBInstanceMessage>}
     *      The data of a new Read Replica DB Instance.
     */
    create_read_replica: (sourceDbInstance={}, params) => {

        params = {
            ..._render_read_replica_params(sourceDbInstance),
            ...params
        };

        console.log({'Read Replica Request Params' : params});

        return new Promise(resolve => resolve(user_requests.confirm_solution(
                `Create a Read Replica DB Instance [${params.DBInstanceIdentifier}] from ` +
                    `a source DB Instance [${sourceDbInstance.DBInstanceIdentifier}]?`
                )
                .then(response => {

                    if (response === false)
                        throw new Error('The solution was NOT confirmed. The process was stopped.');

                    console.log(
                        '>>> Waiting for the DB Instance be in an available state...\n',
                        `DB Instance: ${sourceDbInstance.DBInstanceIdentifier}`
                    );
                }))
            )
            .then(() => aws_requests.rds.wait_for({
                    DBInstanceIdentifier: sourceDbInstance.DBInstanceIdentifier
                })
                .then(() => console.log(
                    `<<< [!] A DB Instance [${sourceDbInstance.DBInstanceIdentifier}] is available now! >>>`
                ))
            )
            .then(() => {

                    console.log(
                        '>>> Requesting to create a Read Replica DB Instance...\n',
                        `DBInstanceIdentifier: [${params.DBInstanceIdentifier}]`
                    );

                    return aws_requests.rds.create_read_replica(params)
                    .then(readReplicaDbInstance => {

                        console.log('<<< [!] A Read Replica DB Instance is creating in AWS RDS! >>>');

                        return readReplicaDbInstance;
                    });
                }
            )
            .then(readReplicaDbInstance => {

                console.log(
                    '>>> Waiting for the DB Instance be in an available state...\n',
                    `DB Instance: ${readReplicaDbInstance.DBInstanceIdentifier}`
                );

                return aws_requests.rds.wait_for( {
                    DBInstanceIdentifier: readReplicaDbInstance.DBInstanceIdentifier
                })
                .then(readReplicaDbInstance => {

                    console.log(
                        `<<< [!] A DB Instance [${readReplicaDbInstance.DBInstanceIdentifier}] is available now! >>>`
                    );

                    return readReplicaDbInstance;
                });
            });
    },

};

// Private Functions:

/**
 * Render Params for Create DB Instance Request.
 */
const _render_create_db_params = () => ({
    DBInstanceClass: 'db.t2.micro', /* required */
    DBInstanceIdentifier: '', /* required <- script asks by USER Input Stream Mode */
    Engine: 'mysql', /* required */
    MasterUsername: 'admin', /* required */
    MasterUserPassword: '', /* required <- script asks by USER Input Stream Mode */
    DBParameterGroupName: 'default.mysql5.6',
    EngineVersion: '5.6.41',
    AllocatedStorage: 20,
    MultiAZ: false,
    Port: 3306,
    AutoMinorVersionUpgrade: false,
    PubliclyAccessible: true,
    // VpcSecurityGroupIds: [
    //    '<SG ID>', /* Security Group ID */
    //]
});

/**
 * Render Params for Create a Read Replica DB Instance Request.
 */
const _render_read_replica_params = (sourceDbInstance) => ({
    DBInstanceIdentifier: '', /* required */
    SourceDBInstanceIdentifier: sourceDbInstance.DBInstanceIdentifier, /* required */
    MultiAZ: false,
    PubliclyAccessible: sourceDbInstance.PubliclyAccessible,
    VpcSecurityGroupIds: sourceDbInstance.VpcSecurityGroups.map(
        key => key.VpcSecurityGroupId
    )
});

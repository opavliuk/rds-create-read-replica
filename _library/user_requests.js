/**
 * User Requests Module.
 *
 * Consists of useful functions and methods for
 * work with user experience.
 */

/**
 * NPM Modules.
 *
 * Documentation:
 * @module readline - https://nodejs.org/api/readline.html
 */
const readline = require('readline');

/**
 * Exports Module Functions.
 */
module.exports = {

    /**
     * User Input.
     * Turns on input stream and read lines.
     *
     * @param {string} promptMsg
     *      A message for prompt of input.
     *
     * @return {Promise<string>}
     *      A input of user.
     */
    user_input: (promptMsg = '') => {

        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.setPrompt(promptMsg);
        rl.prompt();

        return new Promise(resolve => {
            rl.on('line', userInput => {
                if (!userInput)
                {
                    console.log('[!] The input is EMPTY. Write something!');
                    rl.prompt();
                }
                else
                {
                    rl.close();
                    resolve(userInput);
                }
            });
        });
    },

    /**
     * Select Key.
     * Asks the user to select one key from list of keys.
     *
     * @param {string} infoMsg
     *      The information message before asks to select the key.
     * @param {string[]} keys
     *      The list of keys from which need to select one.
     *
     * @return {Promise<string>}
     *      The selected key in the argument.
     */
    select_key: function (infoMsg='', keys=['']) {

        console.log(infoMsg);
        keys.forEach((item, index) => console.log((index + 1) + ": " + item));

        return this.user_input('Please, select ONLY ONE number of keys: ')
            .then(input => {
                let userInput = parseInt(input.toString());

                if (typeof keys[userInput - 1] === 'undefined')
                    return this.select_key(infoMsg, keys);

                return keys[userInput - 1];
            });
    },

    /**
     * Confirm solution.
     * Confirmation of some solution for more security
     * against unconscious behavior.
     *
     * @param {string} solutionMsg
     *      The described solution.
     *
     * @return {Promise<boolean>}
     *      true - is confirmed, false - is NOT confirmed.
     */
    confirm_solution: function (solutionMsg='') {
        return this.select_key(
            `[!] Confirm please your solution: ${solutionMsg}`,
            ['Yes', 'No']
        )
        .then(response => response !== 'No');
    },

    /**
     * Input a New Key.
     * Asks to input a new key which doesn't exist
     * in the list of keys.
     *
     * @param {string} promptMsg
     *      A message for prompt of input.
     * @param {[]} keys
     *      A list of exists keys.
     *
     * @return {Promise<string>}
     *      A name of a NEW key.
     */
    input_new_key: function (promptMsg='', keys=[]) {
        return this.user_input(promptMsg)
            .then(userInput => {

                if (!keys.includes(userInput))
                    return userInput;

                keys.forEach((item) => console.log('- ' + item));

                console.log(`[!] The key ${userInput} ALREADY EXISTS in keys. Input another one!`);

                return this.input_new_key(promptMsg, keys);
            });
    },

    /**
     * Input Password.
     * Asks input twice to confirm the password.
     *
     * @param {integer} passLen
     *      A minimum length of password.
     * @param {string} confirmPass
     *      A password to confirm the input.
     *
     * @return {Promise<string>}
     *      Input password.
     */
    input_passwd: function (passLen = 8, confirmPass = '') {
        return this.user_input(
            'Input a password: '
            )
            .then(userInput => {

                if (userInput.length < passLen)
                {
                    console.log(`[!] The password must have at least ${passLen} characters!`);

                    return this.input_passwd(passLen, confirmPass);
                }

                if (!confirmPass)
                {
                    console.log('Please, input again to confirm the password!');

                    return this.input_passwd(passLen, userInput);
                }
                else if (userInput !== confirmPass)
                    throw new Error('The password was NOT confirmed. Inputs passwords were different!');

                return userInput;
            });
    },

};

/**
 * Local Library Module.
 * Exports ALL modules.
 */
module.exports = {
    // AWS SDK:
    aws_requests: require('./aws_requests'),
    aws_rds: require('./aws_rds'),

    // Other:
    user_requests: require('./user_requests'),
    date_time: require('./date_time'),
    process: require('./process'),
};
